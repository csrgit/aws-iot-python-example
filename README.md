## AWS IoT 
This is the client class used for plain MQTT communication with AWS IoT. 

## Minimum Requirements

Python 2.7+ or Python 3.3+

## How to run AWSIoTMQTTClient

```
$ python client.py

```


Subscribe a new Topic for a test , use the name "myTopic" for this example 

![picture](/img/aws2.png)


Create a topic on aws iot test console and watching the activity

![picture](/img/aws.png)                                                     