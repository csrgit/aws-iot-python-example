# Import SDK packages
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
import json

# For certificate based connection
#myAWSIoTMQTTClient = AWSIoTMQTTClient(clientId)
# For Websocket connection

myMQTTClient = AWSIoTMQTTClient("test")
# Configurations
# For TLS mutual authentication

myMQTTClient.configureEndpoint("endpoint.iot.us-west-2.amazonaws.com", 8883)
# For Websocket
# myMQTTClient.configureEndpoint("YOUR.ENDPOINT", 443)

myMQTTClient.configureCredentials("./certs/root-CA.crt", "./certs/Api.private.key", "./certs/Api.cert.pem")
# For Websocket, we only need to configure the root CA
# myMQTTClient.configureCredentials("YOUR/ROOT/CA/PATH")

myMQTTClient.configureOfflinePublishQueueing(-1)  # Infinite offline Publish queueing
myMQTTClient.configureDrainingFrequency(2)  # Draining: 2 Hz
myMQTTClient.configureConnectDisconnectTimeout(10)  # 10 sec
myMQTTClient.configureMQTTOperationTimeout(5)  # 5 sec

def customcallback(client, userdata, message):
    print("Received a new message: ")
    print(message.payload)
    print("from topic: ")
    print(message.topic)
    print("--------------\n\n")

topic = "myTopic"

messageJson = json.dumps({'serial': '100010','battery': '80'})

myMQTTClient.connect()
myMQTTClient.subscribe("test", 1, customcallback)
myMQTTClient.publish(topic, messageJson, 1)

print messageJson